class edendeploy::wp (
  $siteName = 'New WP Site',
  $username = 'admin',
  $password = 'admin',
  $email = '',
  $mysql_host = '',
  $mysql_user = '',
  $mysql_password = '',
  $mysql_db = '',
  $dbCloneSite = '',
  $dbCloneBasicAuthUser = '',
  $dbCloneBasicAuthPass = '',
  $deploySite = '',
  $migrateKey = '',
  $testEnv = 'local'
)
{

  $deployFolder = 'deploy'
  $deployFile = "$deployFolder/wp_deploy.js"
  $deployConfigFile = "$deployFolder/casperDeployConfig.js"

  file { 'deployFolder' :
    path => "/var/www/$deployFolder",
    ensure => 'directory',
    mode => '777'
  }

  file { 'deployConfig' :
    path => "/var/www/$deployConfigFile",
    content => template('edendeploy/casperDeployConfig.js.erb'),
    ensure => 'present',
    mode => '777',
    require => File['deployFolder']
  }

  exec {
    'getDeployScript' :
      cwd => '/var/www/',
      command => "wget https://bitbucket.org/edentic/deploymentscripts/raw/master/CasperJS/WP/wp_deploy.js -O $deployFile",
      creates => "/var/www/$deployFile",
      require => [Package['wget'], File['deployFolder']];

    'deployWP' :
    cwd => '/var/www/',
    command => "/usr/bin/casperjs test $deployFile --includes=$deployConfigFile",
    logoutput => on_failure,
    require => [Class['casperjs'], File['deployConfig'], Exec['getDeployScript'], Class['apache'], Class['php']];
  }
}