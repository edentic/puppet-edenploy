class edendeploy (
  $deploySystem = 'edendeploy::wp',
  $siteName = 'New WP Site',
  $username = 'admin',
  $password = 'admin',
  $email = '',
  $mysql_host = '',
  $mysql_user = '',
  $mysql_password = '',
  $mysql_db = '',
  $dbCloneSite = '',
  $deploySite = '',
  $migrateKey = '',
  $testEnv = 'local',
  $dbCloneBasicAuthUser = '',
  $dbCloneBasicAuthPass = ''
) {
    class {$deploySystem :
       siteName => $siteName,
       username => $username,
       password => $password,
       email => $email,
       mysql_host => $mysql_host,
       mysql_user => $mysql_user,
       mysql_password => $mysql_password,
       mysql_db => $mysql_db,
       dbCloneSite => $dbCloneSite,
       deploySite => $deploySite,
       migrateKey => $migrateKey,
       testEnv => $testEnv,
       dbCloneBasicAuthUser => $dbCloneBasicAuthUser,
       dbCloneBasicAuthPass => $dbCloneBasicAuthPass
    }
}