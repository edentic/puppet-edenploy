#EdenDeploy Puppet module
Edentic deployment Puppet module, which at the current moment only can handle [WordPress](http://wordpress.org/) deployments.

##Requirements
- [Edentic CasperJS puppet module](https://github.com/Edentic/puppet-casperjs)
- [Puppetlabs Apache Module](https://forge.puppetlabs.com/puppetlabs/apache) - (NGINX will be supported in future releases)
- [PHP module](https://forge.puppetlabs.com/example42/php)

##Installation
Copy the module folder to your Puppet `modules` folder.

##Required parameters
EdenDeploy can be configured with a lot of parameters, but has these required parameters when used:

###Required: Site parameters
- **email** - The email your installation will be using(be aware that this might get overwritten by WP-Migrate-DB-Pro on DB sync)
- **deploySite** - The web address of the deploy site 

*Username and password will default to admin*

###Required: MySQL parameters
- **mysql_host** - Hostname for MySQL server
- **mysql_user** - Username for MySQL server
- **mysql_password** - Password for MySQL server
- **mysql_db** - Name of MySQL database

###Optional: [WP-Migrate-DB-Pro](https://deliciousbrains.com/wp-migrate-db-pro/) settings (copy DB from specified site)
- **dbCloneSite** - Web address from which WP-Migrate-DB-Pro should fetch the DB
- **migrateKey** - The WP-Migrate-DB-Pro migration key (found on the second line under connection settings)
- **dbCloneBasicAuthUser** - If your *dbCloneSite* uses basic auth, you can set the username
- **dbCloneBasicAuthPass** - If your *dbCloneSite* uses basic auth, you can set the password

##Sample usage
In your main manifests file you can add the EdenDeploy module using the following code:

###Sets-up a standard WP installation without migrating DB
```
class {'edendeploy' :
  email => “”,
  deploySite => “”,
  mysql_db => “”,
  mysql_user => “”,
  mysql_password => “”
}
```
###Sets-up a standard WP installation and copy the content from the given clone site using WP-Migrate-DB-Pro (this plugin should be available in your WP plugins folder!)
```
class {'edendeploy' :
  email => “”,
  deploySite => “”,
  mysql_db => “”,
  mysql_user => “”,
  mysql_password => “”,
  dbCloneSite => “”,
  migrateKey => “”,
  dbCloneBasicAuthUser => “”,
  dbCloneBasicAuthPass => “”
}
```